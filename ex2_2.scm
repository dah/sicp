
; line segments
(define make-segment cons)
(define start-segment car)
(define end-segment   cdr)

(define (mid-segment s) 
 (let ((start (start-segment s))
       (end (end-segment s)))
    (make-point 
      (average (x-point start) (x-point end))
      (average (y-point start) (y-point end)))))

(define (print-segment s)
 (newline)
 (display "[")
 (print-point (start-segment s))
 (display ",")
 (print-point (end-segment s))
 (display "]"))

; points
(define make-point cons)
(define x-point car)
(define y-point cdr)
 


(define (print-point p)
 (newline)
 (display "(")
 (display (x-point p))
 (display ",")
 (display (y-point p))
 (display ")"))

; utility
(define (average x y) (/ (+ x y) 2))

; tests
(print-point (mid-segment (make-segment (make-point 0 0) (make-point 10 10))))



