(define (make-account balance password)
 (define (withdraw amount)
  (if (>= balance amount)
   (begin (set! balance (- balance amount))
    balance)
   "Недостаточно денег на счете"))
 (define (deposit amount)
  (set! balance (+ balance amount))
  balance)
 (define (dispatch m in-password)
  (cond 
   ((not (eq? in-password password)) (lambda (x) "Incorrect password"))
   ((eq? m 'withdraw) withdraw)
   ((eq? m 'deposit) deposit)
   (else (error "Неизвестный вызов -- MAKE-ACCOUNT"
          m))))
 dispatch)

(define (make-joint acc acc-password new-password)
  (define (dispatch-adapter m in-password)
    (cond 
    ((not (eq? in-password new-password)) (lambda (x) "Incorrect password"))
    (else (acc m acc-password))))
  dispatch-adapter)


(load "test-manager/load.scm")
(define-test 
 (let*
  ((peter-acc (make-account 100 'open-sesame))
   (paul-acc (make-joint peter-acc 'open-sesame 'rosebud)))
  (assert-equal ((paul-acc 'withdraw 'rosebud) 10) 90)
  (assert-equal ((peter-acc 'withdraw 'open-sesame) 10) 80)))

(run-registered-tests)



