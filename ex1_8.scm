; Newton's method for cube roots is based on the fact that if y is an approximation 
; to the cube root of x, then a better approximation is given by the value:
;  x/y^2 + 2y
; ------------
;      3

(define (cube-root x) 
  (if (= x 0) x
   (cuber-iter 0 (/ x 2.0) x)))

(define (cuber-iter old-guess guess x) 
  (if (good-enough? old-guess guess x)
      guess
      (cuber-iter guess (improve guess x) x)))

(define (improve y x)
  (/ (+ (/ x (* y y)) (* 2 y)) 3))

(define (good-enough? old-guess new-guess x)
  (< (/ (abs (- old-guess new-guess)) x) 0.001))

(cube-root 0.0)
(cube-root 1)
(cube-root 27)
(cube-root 0.0001)
(cube-root (* 100000000 123456789 123456789))
(cube-root (* 0.0005  0.0001 0.0004))

