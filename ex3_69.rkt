#lang planet neil/sicp
(#%require "sicp_streams.rkt")

(define (interleave s1 s2)
  (if (stream-null? s1)
      s2
      (cons-stream (stream-car s1)
                   (interleave s2 (stream-cdr s1)))))

(define (pairs s t)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (interleave
    (stream-map (lambda (x) (list (stream-car s) x))
                (stream-cdr t))
    (pairs (stream-cdr s) (stream-cdr t)))))

; Write a procedure triples that takes three infinite streams, S, T, and U, 
; and produces the stream of triples (Si,Tj,Uk) such that i <= j <= k. 
; Use triples to generate the stream of all Pythagorean triples of positive integers, 
; i.e., the triples (i,j,k) such that i < j and i^2 + j^2 = k^2. 


(define (triples s t u)
  (cons-stream 
   (list (stream-car s) (stream-car t) (stream-car u))
   (interleave
    (stream-map (lambda (x) (cons (stream-car s) x))
                (pairs t u))
    (triples (stream-cdr s) (stream-cdr t) (stream-cdr u)))))

(define (square x) (* x x))

(define pythagorean-triples
  (stream-filter (lambda (x)
                   (and 
                    (< (list-ref x 0) (list-ref x 1))
                    (= (+ (square (list-ref x 0)) (square (list-ref x 1))) (square (list-ref x 2)))))
                 (triples integers integers integers)))
  