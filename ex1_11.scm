; f(n) = n if n < 3
; f(n) = f(n - 1) + 2 f(n - 2) + 3 f(n - 3) if n >= 3

(define (f n) 
  (if (< n 3)
     n
     (+ (f (- n 1))
        (* 2 (f (- n 2)))
        (* 3 (f (- n 3))))))

; f3' <- f3 + 2 f2 + 3 f3
; f2' <- f3
; f1  <- f2
; [0, 1, 2, (2 + 2 * 1 + 3 * 0)]
(define (f2 n) (f-iter 2 1 0 n))

(define (f-iter f2 f1 f0 count)
  (if (= count 0)
    f0
    (f-iter (+ f2  (* 2 f1) (* 3 f0)) f2 f1 (- count 1))))

(f 15)
(f2 15)

