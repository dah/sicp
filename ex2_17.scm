
; returns the list containing only the last element of a given list
(define (last-pair s) 
  (let ((tail (cdr s)))
    (if (null? tail) s (last-pair tail))))

(last-pair (list 23 72 149 34)) ; (34)

