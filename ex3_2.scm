
(define (make-monitored proc) 
  "returns function that keeps track of the number of times it has been called"
  (define counter 0)
  (lambda (arg)
    (cond 
     ((eq? 'how-many-calls? arg) counter)
     ((eq? 'reset-count arg) (set! counter 0))
     (else (begin (set! counter (+ 1 counter)) (proc arg))))))


(load "test-manager/load.scm")
(define-test 
 (let ((s (make-monitored sqrt))
       (t (make-monitored sqrt)))
 (assert= (s 100) 10)
 (assert= (s 'how-many-calls?) 1)
 (assert= (s 100) 10 )
 (assert= (s 'how-many-calls?) 2)
 (assert= (t 'how-many-calls?) 0 )
 (s 'reset-count)
 (assert= 0 (s 'how-many-calls?))))

(run-registered-tests)


