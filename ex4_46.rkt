The order of arguments matters because of impure parse functions. 
For example in the following code evaluation from left to right is required by language semantic : an article comes first, a noun comes second.

  (list 'simple-noun-phrase
        (parse-word articles)
        (parse-word nouns))