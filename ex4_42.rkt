(define (require p)
  (if (not p) (amb)))

(define (true1 a b)
  (if (if a (not b) false)
      true
      (if (not a) b false)))

(define (distinct? items)
  (cond ((null? items) true)
        ((null? (cdr items)) true)
        ((member (car items) (cdr items)) false)
        (else (distinct? (cdr items)))))

(define (liars)
  (let 
      ((betty (amb 1 2 3 4 5))
       (ethel (amb 1 2 3 4 5))
       (joan  (amb 1 2 3 4 5))
       (kitty (amb 1 2 3 4 5))
       (mary (amb 1 2 3 4 5)))
    
    (require (distinct? (list betty ethel joan kitty mary)))
    
    ;Betty: ``Kitty was second in the examination. I was only third.''
    (require (true1 (= kitty 2) (= betty 3)))
    ;Ethel: ``You'll be glad to hear that I was on top. Joan was second.''
    (require (true1 (= ethel 1) (= joan 2)))
    ;Joan: ``I was third, and poor old Ethel was bottom.''
    (require (true1 (= joan 3) (= ethel 5)))
    ;Kitty: ``I came out second. Mary was only fourth.''
    (require (true1 (= kitty 2) (= mary 4)))
    ;Mary: ``I was fourth. Top place was taken by Betty.'' 
    (require (true1 (= mary 4) (= betty 1)))
    
    (list (list 'betty betty)
          (list 'ethel ethel)
          (list 'joan joan)
          (list 'kitty kitty)
          (list 'mary mary))))

;Answer: ((betty 3) (ethel 5) (joan 2) (kitty 1) (mary 4))