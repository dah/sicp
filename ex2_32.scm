(define (subsets s)
 (if (null? s)
  (list ())
  (let ((rest (subsets (cdr s))))
   (append rest (map (lambda (x) (cons (car s) x)) rest)))))


(load "quickcheck.scm")
(define (gen-list-max gen max)
 (map (lambda (x) (gen)) (iota (random max))))

(define (gen-intlist) (gen-list-max gen-int 10))

(subsets (list 1 2 3))

(define (check-len s)
 (let ((n (length s)))
  (= (expt 2 n) (length (subsets s)))))



(for-all check-len gen-intlist)
(for-all (lambda (x) (find (lambda (y) (equal? x y)) (subsets x))) gen-intlist)
(for-all (lambda (x) (find (lambda (y) (null? y)) (subsets x))) gen-intlist)


