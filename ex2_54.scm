(define (equal? as bs) 
  (cond 
   ((and (pair? as) (pair? bs)) (and (equal? (car as) (car bs)) (equal? (cdr as) (cdr bs))))
   ((and (null? as) (null? bs)) #t)
   (else (eq? as bs))))

(load "test-manager/load.scm")
(define-each-test
  (assert-true (equal? '() '() ))
  (assert-true (equal? '(1) '(1) ))
  (assert-true (equal? '(2) '(2) ))
  (assert-true (equal? '(2 (1 2)) '(2 (1 2)) ))
  (assert-false (equal? '(1) '((2)) ))
  (assert-false (equal? '(1) '(2) ))
  (assert-false (equal? '(1) '() ))
  (assert-false (equal? '() '(1) ))
  (assert-false (equal? '() 'a))
  (assert-false (equal? '(a) 'a)))

(run-registered-tests)
