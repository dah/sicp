#lang planet neil/sicp
(#%require "sicp_streams.rkt")

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else
         (let* ((s1car (stream-car s1))
                (s2car (stream-car s2))
                (w1car (weight s1car))
                (w2car (weight s2car)))
           (if (< w1car w2car)
               (cons-stream s1car (merge-weighted (stream-cdr s1) s2 weight))
               (cons-stream s2car (merge-weighted s1 (stream-cdr s2) weight)))))))

(define (weighted-pairs s t weight)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (merge-weighted
    (stream-map (lambda (x) (list (stream-car s) x))
                (stream-cdr t))
    (weighted-pairs (stream-cdr s) (stream-cdr t) weight)
    weight)))

; generate a stream of all numbers that can be written as the sum of two squares in three different ways (showing how they can be so written).
(define (square x) (* x x))
(define (sq-weight x)
  (apply 
   (lambda (i j) (+ (square i) (square j)))
   x))

(define (stream-skip-while f s)
  (if (or (stream-null? s) (f (stream-car s)))
      (stream-skip-while f (stream-cdr s))
      s))

(define (find-sq-triples s weight)
  (if (= (weight (stream-ref s 0)) (weight (stream-ref s 1)) (weight (stream-ref s 2)))
      (cons-stream (list (weight (stream-ref s 0))
                         (stream-ref s 0) (stream-ref s 1) (stream-ref s 2))
                   (find-sq-triples
                    (stream-skip-while (lambda (x) (= (weight x) (weight (stream-ref s 0)))) s) weight))
      (find-sq-triples (stream-cdr s) weight)))

(define S 
  (find-sq-triples 
   (weighted-pairs integers integers sq-weight) 
   sq-weight))

(display-stream (take S 120))

