; binary mobile

(define (make-mobile left right)
 (list left right))

(define left-branch car)
(define right-branch cadr)


; structure is either a number representing a simple weight or another mobile
(define (make-branch length structure)
  (list length structure))

(define branch-length car)
(define branch-structure cadr)

(define (branch-weight branch) 
  (let ((s (branch-structure branch)))
    (if (number? s)
      s
      (total-weight s))))

(define (total-weight s)
 (+ (branch-weight (left-branch s)) (branch-weight (right-branch s))))

(define (mobile-balanced? s) 
 (define (torgue x) (* (branch-weight x) (branch-length x)))
 (define (subtest x) (if (number? (branch-structure x)) #t (mobile-balanced? (branch-structure x))))
 (and 
  (= (torgue (left-branch s)) (torgue (right-branch s)))
  (subtest (left-branch s))
  (subtest (right-branch s))))


;tests
(define m (make-mobile (make-branch 5 3) (make-branch 3 5)))
(total-weight m)
(mobile-balanced? m)


