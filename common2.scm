; sequences
(define nil (list))
(define (enumerate-interval low high)
 (if (> low high)
  nil
  (cons low (enumerate-interval (+ low 1) high))))

(define (flatmap proc seq) 
 (accumulate append nil (map proc seq)))

(define (accumulate op initial sequence)
 (if (null? sequence)
  initial
  (op (car sequence)
   (accumulate op initial (cdr sequence)))))

; numbers
(define (square x) (* x x))

(define (find-divisor n test-divisor)
 (cond ((> (square test-divisor) n) n)
  ((divides? test-divisor n) test-divisor)
  (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b)
 (= (remainder b a) 0))

(define (smallest-divisor n)
 (find-divisor n 2))

(define (prime? n)
 (= n (smallest-divisor n)))



