(define (make-interval a b) (cons a b))
(define lower-bound car)
(define upper-bound cdr)

(define (add-interval x y)
 (make-interval (+ (lower-bound x) (lower-bound y))
                (+ (upper-bound x) (upper-bound y))))

; -- -- (lower = upper1 * upper2, upper = lower1 * lower2)
; -- -+ (lower = lower1 * upper2, upper = lower1 * lower2) upper1 is not used
; -- ++ (lower = upper2 * lower1, upper = lower2 * upper1)
; -+ -- (lower = lower2 * upper1, upper = lower2 * lower1) upper2 is not used

; -+ -+ (lower = min(lower1 * upper2, upper1 * lower2), upper = max(lower1 * lower2, upper1 * upper2)) 

; -+ ++ (lower = upper2 * lower1, upper = upper2 * upper1) lower2 is not used
; ++ -- (lower = upper1 * lower2, upper = lower1 * upper2)
; ++ -+ (lower = upper1 * lower2, upper = upper1 * upper2) lower1 is not used
; ++ ++ (lower = lower1 * lower2, upper = upper1 * upper2)


(define (mul-interval x y)
(let ((p1 (* (lower-bound x) (lower-bound y))) 
      (p2 (* (lower-bound x) (upper-bound y)))
      (p3 (* (upper-bound x) (lower-bound y)))
      (p4 (* (upper-bound x) (upper-bound y))))
 (make-interval (min p1 p2 p3 p4)
                (max p1 p2 p3 p4))))

(define (div-interval x y)
 (if (and (<= (lower-bound y) 0) (<= 0 (upper-bound y)))
   (error "denominator spans zero")
   (mul-interval x
    (make-interval (/ 1.0 (upper-bound y))
                   (/ 1.0 (lower-bound y))))))


(define (sub-interval x y)
  (make-interval (- (lower-bound x) (upper-bound y))
                 (- (upper-bound x) (lower-bound y))))

;tests
(sub-interval (make-interval 10 100) (make-interval 1 100)) ; [-90, 99]
;(div-interval (make-interval 1 10) (make-interval -1 1))

