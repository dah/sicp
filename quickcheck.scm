(load-option 'format)
(define (gen-int)
 (random 256))

(define (gen-bool)
 (= (random 2) 0))

(define (gen-char)
 (integer->char (random 128)))

(define (gen-list gen)
 (map (lambda (x) (gen)) (iota (random 100))))

(define (gen-string)
 (list->string (gen-list gen-char)))

(define (for-all property . generators)
 (let ((values (map (lambda (x) (map (lambda (f) (f)) generators)) (iota 100))))
  (let ((failure (find (lambda (vs) (not (apply property vs))) values)))
   (if (list? failure)
    (display (format #t "*** Failed!\n~a\n" failure))
    (display (format #t "+++ OK, passed 100 tests\n"))))))



;;; Examples:

; (define (gen-even)
;  (let ((n (gen-int)))
;   (if (not (= 0 (modulo n 2)))
;    (+ n 1)
;    n)))
; 
; (define (reversible s)
;  (string=? s (list->string (reverse (reverse (string->list s))))))
; 
; (for-all even? gen-int)
; (for-all even? gen-even)
; (for-all reversible gen-string)
; (exit)
