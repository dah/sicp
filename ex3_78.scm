(load "sicp_streams.scm")
(define (scale-stream s k) (stream-map (lambda (x) (* x k)) s))


(define (integral delayed-integrand initial-value dt)
  (cons-stream initial-value
    (let ((integrand (force delayed-integrand)))
               (if (stream-null? integrand)
                   the-empty-stream
                   (integral (delay (stream-cdr integrand))
                             (+ (* dt (stream-car integrand))
                                initial-value)
                             dt)))))

(define (solve-2nd a b y0 dy0 dt)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay ddy) dy0  dt))
  (define ddy (add-streams (scale-stream dy a) (scale-stream y b)))
  y)

(display (stream-ref (solve-2nd 3 5 1 1 0.001) 1000))

