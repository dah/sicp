(define (sign x) (if (< x 0) -1 1))

(define (make-rat n d)
 (let ((g (gcd n d))
       (s (* (sign n) (sign d))))
  (cons (* s (abs (/ n g))) (abs (/ d g)))))

(define numer car)
(define denom cdr)

(define (print-rat x)
 (newline)
 (display (numer x))
 (display "/")
 (display (denom x)))

(print-rat (make-rat 8 2))
(print-rat (make-rat -8 2))
(print-rat (make-rat 8 -2))
(print-rat (make-rat -8 -2))

