#lang planet neil/sicp
(#%require "sicp_streams.rkt")


(define (rand-update x) (+ x 1))
(define (random-init) 0)
(define (random-seed k) k)

(define (rand actions)
  (define s
    (cons-stream
     (random-init)
     (stream-map
      (lambda (action x)
        (cond 
          ((eq? action 'generate) (rand-update x))
          ((and (pair? action) (eq? (car action) 'reset)) (random-seed (cadr action)))
          (else (error "invalid method -- RAND"))))
      actions
      s)))
  s)

(display-stream (rand (list->stream '(generate generate generate (reset 100) generate generate))))