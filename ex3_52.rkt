#lang planet neil/sicp
(#%require "sicp_streams.rkt")

(define sum 0)

(define (accum x)
  (set! sum (+ x sum))
  sum)

(define seq (stream-map accum (stream-enumerate-interval 1 20)))
; seq = 1, 3, 6, 10, 15, ..
; 1 + 0
; 2 + 1 = 3
; 3 + 3 = 6
; 4 + 6 = 10
; 5 + 10 = 15
; 6 + 15 = 21
; ...
; 20
sum ;1

(define y (stream-filter even? seq)) 
; y = 6, 10, ...
sum ; 6

(define z (stream-filter (lambda (x) (= (remainder x 5) 0))
                         seq))
sum ; 10

; seq(n) = n*(n + 1) / 2
; y[i] = seq(2*i + 1) if i is even
; y[i] = seq(2*i + 2) otherwise
; ...
; y[7] = seq(16)= 16 * (16 + 1) /2  = 8 * 17 = 136
(stream-ref y 7)
sum ; 136

(display-stream z)
sum ; 210
