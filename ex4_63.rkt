(assert! (son Adam Cain))
(assert! (son Cain Enoch))
(assert! (son Enoch Irad))
(assert! (son Irad Mehujael))
(assert! (son Mehujael Methushael))
(assert! (son Methushael Lamech))
(assert! (wife Lamech Ada))
(assert! (son Ada Jabal))
(assert! (son Ada Jubal))

(assert! (rule (son2 ?M ?S)
               (or (son ?M ?S)
                   (and (wife ?M ?W)
                        (son ?W ?S)))))

(assert! (rule (grandson ?G ?S)
               (and (son2 ?F ?S)
                    (son2 ?G ?F))))


(grandson Cain ?x)
; (grandson Cain Irad)

(son2 Lamech ?x)
; (son2 Lamech Jubal)
; (son2 Lamech Jabal)

(grandson Methushael ?x)
; (grandson Methushael Jubal)
; (grandson Methushael Jabal)



