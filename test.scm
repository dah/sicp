(load "test-manager/load.scm")

(define-each-test
  (assert-equal (list) (reverse (list)))
  (assert-equal (list 1) (reverse (list 1)))
  (assert-equal (list 3 2 1) (reverse (list 1 2 3))))

(run-registered-tests)
