; The good-enough? test used in computing square roots will not be very
; effective for finding the square roots of very small numbers. Also, in
; real computers, arithmetic operations are almost always performed with limited precision.
; This makes our test inadequate for very large numbers. Explain these statements,
; with examples showing how the test fails for small and large numbers. An alternative strategy
; for implementing good-enough? is to watch how guess changes from one iteration to the next 
; and to stop when the change is a very small fraction of the guess. Design a square-root
; procedure that uses this kind of end test. Does this work better for small and large numbers? 
(define (sqrt x) 
  (if (= x 0) x
   (sqrt-iter 0 (/ x 2.0) x)))

(define (sqrt-iter old-guess guess x) 
  (if (good-enough? old-guess guess x)
      guess
      (sqrt-iter guess (improve guess x) x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (good-enough? old-guess new-guess x)
  (< (/ (abs (- old-guess new-guess)) x) 0.001))

(define (average x y) (/ (+ x y) 2))

(sqrt 0.0)
(sqrt 1)
(sqrt 9)
(sqrt 0.0001)
(sqrt (* 100000000 123456789))
(sqrt (* 0.0005  0.0001))

