; returns a list containing all leaves of the tree
(define (fringe t) 
 (cond 
  ((pair? t) (append (fringe (car t)) (fringe (cdr t))))
  ((null? t) t)
  (else (list t))))

; test
(define x (list (list 1 2) (list 3 4))) 
(fringe (list x x))

