#lang planet neil/sicp
(#%require "sicp_streams.rkt")

(define (scale-stream s k) (stream-map (lambda (x) (* x k)) s))
(define (partial-sums s)
  (cons-stream (stream-car s) (stream-map (lambda (x) (+ x (stream-car s))) (partial-sums (stream-cdr s)))))

(define (square x) (* x x))

(define (len2-summands n)
  (cons-stream (/ 1.0 n)
               (stream-map - (len2-summands (+ n 1)))))
(define len2-stream
  (partial-sums (len2-summands 1)))
(display-stream (take len2-stream 10))

(define (euler-transform s)
  (let ((s0 (stream-ref s 0))           ; Sn-1
        (s1 (stream-ref s 1))           ; Sn
        (s2 (stream-ref s 2)))          ; Sn+1
    (cons-stream (- s2 (/ (square (- s2 s1))
                          (+ s0 (* -2 s1) s2)))
                 (euler-transform (stream-cdr s)))))


(display-stream (take (euler-transform len2-stream) 10))

(define (make-tableau transform s)
  (cons-stream s
               (make-tableau transform
                             (transform s))))

(define (accelerated-sequence transform s)
  (stream-map stream-car
              (make-tableau transform s)))
               
(display-stream (take (accelerated-sequence euler-transform
                                      len2-stream) 20))