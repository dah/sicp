(define (sum-of-squares x y)
  (define (sqr x) (* x x))
  (+ (sqr x) (sqr y)))

; calculates sum of two largest numbers
(define (sum-of-squares3 x y z) 
 (sum-of-squares (max y x) (max (min y x) z)))


; xy  yx
; yz  zy
; xz  zx
(sum-of-squares3 0 0 0) ; 0

(sum-of-squares3 1 2 0)
(sum-of-squares3 2 1 0)

(sum-of-squares3 1 0 2)
(sum-of-squares3 2 0 1)

(sum-of-squares3 0 1 2)
(sum-of-squares3 0 2 1)

