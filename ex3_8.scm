

(define g 0)
(define (f n) (begin (set! g n) g))

(+ (f 0) (f 1))

