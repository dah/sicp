#lang planet neil/sicp
(#%require "sicp_streams.rkt")


(define (partial-sums s)
  (cons-stream (stream-car s) (stream-map (lambda (x) (+ x (stream-car s))) (partial-sums (stream-cdr s)))))

(define x (partial-sums integers))                                                  