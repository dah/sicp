; returns the list with its elements reversed and all sublists deep-reversed as well
(define (deep-reverse s) 
  (reverse (map (lambda (x) (if (pair? x) (deep-reverse x) x)) s)))

; test
(deep-reverse (list (list 1 2) (list 3 4))) ; (4 3) (2 1)

