(define (make-account balance password)
 (define (withdraw amount)
  (if (>= balance amount)
   (begin (set! balance (- balance amount))
    balance)
   "Недостаточно денег на счете"))
 (define (deposit amount)
  (set! balance (+ balance amount))
  balance)
 (define (dispatch m in-password)
  (cond 
   ((not (eq? in-password password)) (lambda (x) "Incorrect password"))
   ((eq? m 'withdraw) withdraw)
   ((eq? m 'deposit) deposit)
   (else (error "Неизвестный вызов -- MAKE-ACCOUNT"
          m))))
 dispatch)


(define acc (make-account 100 'pwd))
((acc 'withdraw 'wrong-pwd) 10)
((acc 'withdraw 'pwd) 10)


