(define (myreverse s)
 (define (reverse-iter s r)
  (if (null? s)
    r
    (reverse-iter (cdr s) (cons (car s) r))))
 (reverse-iter s ()))
 

; test
(myreverse ())
(myreverse (list 1))
(myreverse (list 1 4 9 16 25)) ; (25 16 9 4 1)

