(load "common2.scm")
; generates the sequence of pairs (i, j) where 1 <= j < i <= n
(define (unique-pairs n)  
  (flatmap (lambda (i) 
        (map (lambda (j) (list i j)) (enumerate-interval 1 (- i 1))))
   (enumerate-interval 1 n)))

(define (prime-sum-pairs n)
 (map make-pair-sum
  (filter prime-sum? (unique-pairs n))))


(define (make-pair-sum pair)
 (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))

(define (prime-sum? pair)
 (prime? (+ (car pair) (cadr pair))))


(load "test-manager/load.scm")
(define-each-test
  (assert-equal (list) (unique-pairs 1))
  (assert-equal (list (list 2 1)) (unique-pairs 2))
  (assert-equal (list (list 2 1) (list 3 1) (list 3 2)) (unique-pairs 3)))

(define-test 
 (assert-equal '((2 1 3) (3 2 5) (4 1 5) (4 3 7)) (prime-sum-pairs 4)))

(run-registered-tests)

