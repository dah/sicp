-- The good-enough? test used in computing square roots will not be very
-- effective for finding the square roots of very small numbers. Also, in
-- real computers, arithmetic operations are almost always performed with limited precision.
-- This makes our test inadequate for very large numbers. Explain these statements,
-- with examples showing how the test fails for small and large numbers. An alternative strategy
-- for implementing good-enough? is to watch how guess changes from one iteration to the next 
-- and to stop when the change is a very small fraction of the guess. Design a square-root
-- procedure that uses this kind of end test. Does this work better for small and large numbers? 
import Data.List (find)
import Data.Maybe (fromJust)

average x y = (x + y) / 2.0

mysqrt :: (Floating a, Ord a) => a -> a
mysqrt x = 
  if x == 0
    then x
    else fromJust $ find good_enough $ iterate improve x
  where
    good_enough guess = abs ((guess * guess - x) / x) < 0.0001
    improve guess = average guess (x / guess)



test = 
  [ (mysqrt 0.0)
  , (mysqrt 1.0)
  , (mysqrt 9.0)
  , (mysqrt 0.0001)
  , (mysqrt (100000000.0 * 123456789.0))
  , (mysqrt (0.0005 * 0.0001))
  ]

