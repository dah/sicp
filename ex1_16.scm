; b ^ n  = (b ^ (n / 2)) ^ 2 if n is even
; b ^ n = b * b ^ (n - 1)    if n is odd


; code
(define (fast-expt b n)
 (define (iter a b n) 
  (cond 
   ((= n 0) a)
   ((even? n) (iter a (* b b) (/ n 2)))
   (else (iter (* a b) b (- n 1)))))
 (iter 1 b n))


; tests
(load "test-manager/load.scm")
(define-each-test 
  (assert= 1 (fast-expt 2 0))
  (assert= 2 (fast-expt 2 1))
  (assert= (expt 2 8) (fast-expt 2 8))
  (assert= (expt 2 6) (fast-expt 2 6)))

(run-registered-tests)
