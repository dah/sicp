(load "common2.scm")
; generates the sequence of triples (i, j, k) where 1 <= k < j < i <= n
(define (unique-triples n)  
 (flatmap (lambda (i) 
           (flatmap (lambda (j) 
                 (map (lambda (k) (list i j k))
                       (enumerate-interval 1 (- j 1))))
                  (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))


(define (sum-triples n s)
  (filter (lambda (x) (= (accumulate + 0 x) s)) (unique-triples n)))


(unique-triples 5)
(sum-triples 5 10)

(define (prop-checksum sum) 
 (let ((triple-list (sum-triples 10 sum)))
  (accumulate (lambda (x y) (and x y)) #t
   (map (lambda (triple) (= sum (accumulate + 0 triple))) triple-list))))

(load "quickcheck.scm")
(for-all prop-checksum gen-int)
