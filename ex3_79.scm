(load "sicp_streams.scm")
(define (scale-stream s k) (stream-map (lambda (x) (* x k)) s))


(define (integral delayed-integrand initial-value dt)
  (cons-stream initial-value
    (let ((integrand (force delayed-integrand)))
               (if (stream-null? integrand)
                   the-empty-stream
                   (integral (delay (stream-cdr integrand))
                             (+ (* dt (stream-car integrand))
                                initial-value)
                             dt)))))

(define (solve-2nd f y0 dy0 dt)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay ddy) dy0 dt))
  (define ddy (stream-map f dy y))
  y)


(display (stream-ref (solve-2nd (lambda (dy y) (+ (* 2 dy) (* 3 y))) 1 1 0.0001) 1000))
