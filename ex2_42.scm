(load "common2.scm")

(define (queens board-size)
 (define (queen-cols k)
  (if (= k 0)
   (list empty-board)
   (filter
    (lambda (positions) (safe? k positions))
    (flatmap
     (lambda (rest-of-queens)
      (map (lambda (new-row)
            (adjoin-position new-row k rest-of-queens))
       (enumerate-interval 1 board-size)))
     (queen-cols (- k 1))))))
 (queen-cols board-size))

(define empty-board nil)
(define (adjoin-position new-row k rest-of-queens) (cons (make-pos new-row k) rest-of-queens))

(define make-pos cons)
(define pos-row car)
(define pos-col cdr)

; vertical is always safe 
; check horizontal and diagonal
(define (safe? k positions)
 (define (compare-diags a b)
  (or 
   (= (- (pos-row a) (pos-col a)) (- (pos-row b) (pos-col b)))
   (= (+ (pos-row a) (pos-col a)) (+ (pos-row b) (pos-col b)))))
 (if (= k 1) 
  #t
  (let ((lastpos (car positions)))
  (not (or
   (find (lambda (x) (= (pos-row x) (pos-row lastpos))) (cdr positions))
   (find (lambda (x) (compare-diags x lastpos)) (cdr positions)))))))
        


(queens 8)
(safe? 3 '( (3 . 3) (2 . 2) (1 . 1)))


