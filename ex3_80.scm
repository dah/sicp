(load "sicp_streams.scm")
(define (scale-stream s k) (stream-map (lambda (x) (* x k)) s))


(define (integral delayed-integrand initial-value dt)
  (cons-stream initial-value
    (let ((integrand (force delayed-integrand)))
               (if (stream-null? integrand)
                   the-empty-stream
                   (integral (delay (stream-cdr integrand))
                             (+ (* dt (stream-car integrand))
                                initial-value)
                             dt)))))



(define (RLC R L C dt)
  (define (proc vC0 iL0)
    (define vC (integral (delay dvC) vC0 dt))
    (define iL (integral (delay diL) iL0 dt))

    (define dvC (scale-stream iL (/ -1 C)))
    (define diL (add-streams 
      (scale-stream vC (/ 1 L))
      (scale-stream iL (/ (- R) L))))

    (cons vC iL))
  proc)


; Using RLC, generate the pair of streams that models the behavior of a series RLC circuit with R = 1 ohm, C = 0.2 farad, L = 1 henry, dt = 0.1 second, and initial values iL0 = 0 amps and vC0 = 10 volts. 
(define pair ((RLC 1 1 0.2 0.1) 10 0))
(display-stream (take (car pair) 10))
(display-stream (take (cdr pair) 10))
