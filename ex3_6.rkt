#lang racket


(define (rand-update x) (random 100))
(define (random-init) (random-seed 0))

(define rand
  (let ((x random-init))
    (lambda (m)
      (cond 
        ((eq? m 'generate)
         (begin (set! x (rand-update x)) x))
        ((eq? m 'reset) 
         (lambda (k) (set! x (random-seed k))))
        (else (error "unknown method -- RAND"))))))

((rand 'reset) 0)
(rand 'generate)