#lang planet neil/sicp
(#%require "sicp_streams.rkt")

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else
         (let* ((s1car (stream-car s1))
                (s2car (stream-car s2))
                (w1car (weight s1car))
                (w2car (weight s2car)))
           (if (< w1car w2car)
               (cons-stream s1car (merge-weighted (stream-cdr s1) s2 weight))
               (cons-stream s2car (merge-weighted s1 (stream-cdr s2) weight)))))))

(define (weighted-pairs s t weight)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (merge-weighted
    (stream-map (lambda (x) (list (stream-car s) x))
                (stream-cdr t))
    (weighted-pairs (stream-cdr s) (stream-cdr t) weight)
    weight)))

; a. the stream of all pairs of positive integers (i,j) with i <= j ordered according to the sum i + j
(define Sa (weighted-pairs integers integers (lambda (x) (apply + x))))
(display-stream (take Sa 10))

; b. the stream of all pairs of positive integers (i,j) with i <= j, where 
; neither i nor j is divisible by 2, 3, or 5, and the pairs are ordered according to the sum 2 i + 3 j + 5 i j. 
(define (weight i j) (+ (* 2 i) (* 3 j) (* 5 i j)))
(define Sb-base (stream-filter 
                 (lambda (x) 
                   (and 
                    (> (remainder x 2) 0)
                    (> (remainder x 3) 0)
                    (> (remainder x 5) 0))) integers))

(define Sb
  (weighted-pairs Sb-base Sb-base 
                  (lambda (x) (apply weight x))))
(display-stream (take Sb 30))
