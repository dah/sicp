(define (make-accumulator sum)
 (lambda (n) 
  (begin
   (set! sum (+ sum n))
   sum)))

(load "test-manager/load.scm")
(define-test
 (let ((A (make-accumulator 5)))
  (assert= 15 (A 10 ))
  (assert= 25 (A 10 ))))

(run-registered-tests)
